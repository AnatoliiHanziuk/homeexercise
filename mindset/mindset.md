## TLDR
I cannot dive into details of each of the situations due to NDA. Because of that following answers could sound a bit broad.

## Identified a problem or inefficiency in a process related to quality or development, and implemented a change to improve it
When I joined one of the projects, engineers in the team
- didn't trust the results of test executions because the tests were flaky
- didn't really understand what these tests do because of lack of good reporting
- were focused on developing features without having good understanding who and how will use them
- during the code review put a lot of attention on styleguide, language constructions instead of thinking about the corner cases, what the feature should do and whether the solution is testable

I provided the team with the instrument that finds the flaky tests and, depending on the priority of the test, either automatically moves it into quarantine until they're fixed or notifies the team about flakiness. 
It required having priorities set for all automated tests. Isolating flaky tests and notifying the team about it helped to make the deployment process more predictable, stable and straightforward. Developers stopped creation of workaround to deploy the changes without passing the quality gates.  

I tackled the problem with the reporting recreating this layer in test automation solution. New reporting allowed team members to show the pass/failure trend of the tests over time, have useful additional information about the failure in the report (screenshots, stack traces), see how the tests are split between priorities and features,
how effectively they're executed (what threads are used, whether some threads have been waiting for some time for a new test, etc). I made this report accessible to the team members with a one click without a need to go to the results of the pipeline and search for the test execution results there.

Together with the product owner, we came up with the list of product metrics the team should take care of. I created monitoring, alerting and added a bit of observability on top of it.
Monitoring of product metrics was added to the checklist of actions being done after the release. 
I created visualizations to show to the team who, how and when do use the solution we were working on and what actionable technical and product insights we can get from this information.
If you don't want to be woken up at night or outside of working out when you're on call, you'll need to take care about product metrics too.
I triggered the process of static code analysis introduction and helped engineers to integrate it in CI pipeline and forget about most of the discussions they had previously in the pull requests.
The establishment of product-oriented mindset is ongoing.

As a result of all these initiatives, I received trust of the product owner and engineering team. I made it possible to have CI/CD for the product we were working on. I returned trust to the automated tests because the team knew if some test is failed and there is no notification about its flakiness, 
then there is an easy way to see the root cause of failure (new reporting) and, probably, some issue was introduced during the last change. And it's possible to find the root cause of the issue faster (see answer on the last question, where I'm writing about logging/monitoring/alerting/observability).

## Built or implemented new testing tools or frameworks
Working on one of the applications, it was required to enable the team to receive the faster feedback on a change.  

So I created solutions for the Api and Ui tests on integration and system levels as well as solution for performance and visual regression tests.
In AWS I created the infrastructure as a code for these solutions to operate on. I created CI/CD pipelines and included these solutions to the proper stages of the release process.
After some time I figured out that created solution and especially infrastructure doesn't require any intervention - it's efficient and self-healed in case of any issues. 
So I decided to share this success with the rest of the organization and organized set of demo sessions where I presented these solutions. 
After these presentations several team became interested in adoption of these solutions. During some time I had been helping software engineers as well as quality engineers to execute infrastructure setup in their AWS accounts 
as well as to write multiple tests in the solutions I proposed for their needs. After that I had been reviewing changes for the solutions I developed and was focusing on discussions of architectural updates to these solutions.  

As a result, I created 4 "frameworks" and provided 4 teams with the full ownership of maintaining and expanding these solutions saving a lot of their time to build similar solutions on their own.

## Recognised a pattern of bugs repeating and implemented a change to stop the pattern from recurring
I figured out that from time to time the application my team was working on responded slowly and some portion of information was not displayed at all.
However, there were smoke performance tests being executed, and they shew that the performance of the application should be acceptable.  

I dived deep into the flow that the request takes to prepare result and display it to a user.
I figured out that when the performance is not good, there are CPU spikes in the database cluster. Also, these spikes correlated with CPU spikes on the side of the service (backend) the client was talking to.
Looking on this correlation, I applied circuit breaker pattern to not wait for hanging calculations on the database side. It improved stability of the service (there were no CPU spikes and unhealthy nodes anymore).
Additionally, I added missing logging, monitoring, alerting for database and server as well as introduced Tech Ops review process that takes place once per week, where the team goes through the operational health of the service
and discusses the appeared and potential issues as well as mechanisms to spot these issues on the earlier stages on SDLC. 
After some time, we received an incident regarding CPU spike on database cluster that caused partial unavailability of the application for multiple hours. 
I was doing the root cause investigation. Previously all similar incidents were just fixed and forgotten. 
I brought to the team a culture of writing postmortem documents for high and critical issues. Investigating the root cause, I understood that monitoring database cluster we were looking on incorrect metrics,
the observability and alerting about the issues were inefficient (they displayed some technical information that was not actionable) and the team didn't have the proper culture of reaction on the incidents (there were no accountability).
I had a conversation with engineering manager, shared my observations and proposed the checklist that will help team not to miss the important issues. Especially for the person that is on call.
During the review of postmortem we had a discussion in the team, added several items to the checklist and polished it. After that I shared the finding I made and postmortem I created with the rest of the organization so other teams will be aware about mistakes we made, learn on them and, possibly, fix same mistakes on their side not to face same issues as we did in the future.

As a result, every team member received a common understanding on what activities does he need to do during on call,
what metrics and alarms are the most important, what are the probable root causes of raised alarms and how to mitigate them. Interested engineers from my part of organization also received helpful information applied into practice and recipes of mitigation of issues with CPU spikes in Aurora cluster.

Also, I added missing monitoring and observability to mitigate issues with CPU spikes on the database side faster. 
During the investigation I figured out the reason of non-failed smoke performance tests, shared this reason with a team and propose one of the team members to fix it during the pair programming session.  

As a result, this team member became fully comfortable with creation and adjustment of performance tests and created similar tests suite for nightly builds. 
I shared the ownership related to the solution I created and removed the possible bottleneck.

## When you are testing a new feature, under what circumstances would you deviate from a test script while performing manual testing?
When 
- the test script was not updated for a long time, is unreliable, doesn't represent the real behaviour of the system
- the test script focuses on how application does something without clarifying what the user should/should not achieve with the steps in the test script
- I figure out deviations in technical/product metrics so that I'll dive deep and will try to find the reasons of these deviations
- the test script was written before the feature was implemented and during it's implementation the team agreed to change the feature behaviour
- the test script doesn't have needed details( e.g is unspecific and non-deterministic)

## If you joined a team of 10 developers as the only Quality Engineer and could implement and change any process(es) you’d like, how would you ensure that the team delivers high quality software?
Before implementing any change I need to understand the current state of things and whe one or another decisions were previously made. Without being into context it's impossible to propose solver bullets.
So let's imagine, that there was not a lot of processes being implemented, and I'm empowered to integrate any processes I think will bring profit.

Firstly, the team should be on the same page before it starts the feature implementation. I'll propose to integrate such techniques as 
- having clear user stories with INVEST acceptance criteria
- examples mapping for user stories
- definition of done with quality gates (e.g. each high and critical acceptance criteria should be covered with automated tests; tech and product metrics for the story deployed to prod should be monitored for X hours, etc)

But before that the team should have the shared understanding of quality. To do it, it's required to understand what the team could consider as a deliverable of acceptable quality so the team needs to think about product metrics too.
Any of the high and critical issues shouldn't be forgotten without actions taken to mitigate similar problems in the future. Postmortems and TechOps review could be helpful mechanisms in this case. Read more about them in my previous answers.
Also, if the amount of existing open issues is not big, I encourage to follow zero bug policy to reduce amount of "open mental loops".
I tend to delegate as many of "ensure-related" work to the machines. It includes:
- code review process (static code analysis, checklist for the code review)
- automatic quality gates on different levels of CI/CD pipeline
- management of flaky tests

Then we need to make sure we develop product on only in the right way, but we do the right things for the stakeholders.
Exploratory testing, A/B testing, automatic monitoring and observability, gathering from the feedback on the product features are the best friends in this case.

Also every team member should be able to fix the product or part of the product if something goes wrong. Regular experiments that break existing fuctionality in controlled way (chaos engineering) could help in this case. 
These exercises could be helpful in mature teams to improve the observability of the application, reduce time needed to find the root cause of the issue and fix it and deploy to production.

 

