package com.atlassian.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileUtils {
    public static final Logger log = LoggerFactory.getLogger(FileUtils.class);

    private FileUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static void writeToFile(String filePath, List<String> context, Charset charset) {
        var file = Paths.get(filePath);
        try {
            Files.write(file, context, charset);
        } catch (IOException e) {
            log.warn("Cannot write to a file: {}", e);
        }
    }
}
