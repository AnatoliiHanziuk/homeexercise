package com.atlassian;

public enum RestrictionMode {
    ONLY_SOME_CAN_VIEW_AND_EDIT("Only specific people can view and edit"),
    ANYONE_CAN_VIEW_AND_EDIT("Anyone on Confluence can view and edit"),
    ANYONE_CAN_VIEW_SOME_CAN_EDIT("Anyone on Confluence can view, some can edit");

    private final String mode;

    RestrictionMode(String mode) {
        this.mode = mode;
    }

    public String get() {
        return mode;
    }
}
