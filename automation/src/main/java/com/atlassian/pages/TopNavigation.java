package com.atlassian.pages;

import com.atlassian.pages.confluence.ConfluenceSpaceHomePage;
import com.codeborne.selenide.Condition;

import static com.codeborne.selenide.Selectors.withText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class TopNavigation {
    public ConfluenceSpaceHomePage openSpace(final String name) {
        $("[data-testid='app-navigation-primary-spaces-dropdown']").click();
        $("[data-testid='spaces-dropdown']").$(withText("View all spaces")).shouldBe(Condition.visible);
        $$("[data-testid='pages-dropdown-navigation-item']")
                .stream()
                .filter(space -> space.text().equals(name))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new)
                .click();
        return new ConfluenceSpaceHomePage();
    }
}
