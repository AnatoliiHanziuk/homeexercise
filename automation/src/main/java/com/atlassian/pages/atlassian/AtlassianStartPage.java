package com.atlassian.pages.atlassian;

import com.atlassian.pages.confluence.ConfluenceHomePage;
import io.qameta.allure.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.codeborne.selenide.Selenide.$;

public class AtlassianStartPage {
    public static final Logger log = LoggerFactory.getLogger(AtlassianStartPage.class);

    @Step("Open Confluence")
    public ConfluenceHomePage openConfluence() {
        log.info("Open Confluence");
        $("[aria-label='Confluence']").click();
        return new ConfluenceHomePage();
    }
}
