package com.atlassian.pages.atlassian;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.codeborne.selenide.Selenide.$;

public class AtlassianLoginPage {
    public static final Logger log = LoggerFactory.getLogger(AtlassianLoginPage.class);

    @Step("Open Atlassian login page")
    public AtlassianLoginPage open() {
        log.info("Open Atlassian login page");
        Selenide.open("/login");
        return this;
    }

    @Step("Login to Atlassian")
    public AtlassianStartPage login(final String email, final String password) {
        log.info("Login to Atlassian account");
        $("#username").val(email);
        $("#login-submit").click();
        $("#password").val(password);
        $("#login-submit").click();
        return new AtlassianStartPage();
    }
}
