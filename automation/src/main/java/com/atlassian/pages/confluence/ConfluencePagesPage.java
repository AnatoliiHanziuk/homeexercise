package com.atlassian.pages.confluence;

import com.codeborne.selenide.Condition;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ConfluencePagesPage implements ConfluenceBasePage {

    @Override
    public String getTitle() {
        return "Pages";
    }

    public ConfluencePage search(final String name) {
        $("[aria-label='Search by title']").val(name);
        $$("[data-testid='page-card-grid-view']")
                .filterBy(Condition.text(name))
                .first()
                .click();
        return new ConfluencePage();
    }
}
