package com.atlassian.pages.confluence;

import com.atlassian.pages.TopNavigation;
import io.qameta.allure.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.codeborne.selenide.Selenide.$;

public class ConfluenceHomePage implements ConfluenceBasePage {
    public static final Logger log = LoggerFactory.getLogger(ConfluenceHomePage.class);

    private final TopNavigation topNavigation;

    public ConfluenceHomePage() {
        topNavigation = new TopNavigation();
    }

    @Step("Open '{name}' space")
    public ConfluenceSpaceHomePage openSpace(final String name) {
        log.info("Open space {}", name);
        return topNavigation.openSpace(name);
    }

    @Override
    public String getTitle() {
        return $("[data-testid='space-section']").text();
    }
}
