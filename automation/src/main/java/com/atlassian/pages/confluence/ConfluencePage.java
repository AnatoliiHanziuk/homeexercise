package com.atlassian.pages.confluence;

import com.atlassian.RestrictionMode;
import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.codeborne.selenide.Selenide.$;

public class ConfluencePage implements ConfluenceBasePage {
    public static final Logger log = LoggerFactory.getLogger(ConfluencePage.class);

    @Step("Modify page restrictions")
    public ConfluencePage setRestriction(final RestrictionMode mode) {
        log.info("Modify page restriction mode to {}", mode.get());
        modifyRestrictions().selectRestriction(mode);
        reload();
        return this;
    }

    @Step("Get selected restriction mode")
    public RestrictionMode getRestrictionMode() {
        log.info("Get restriction mode of the page");

        final String restrictionsButtonSelector = "[data-test-id='restrictions.dialog.button']";
        $(restrictionsButtonSelector).shouldBe(Condition.visible);

        if ($(restrictionsButtonSelector + "[aria-label='No restrictions']").isDisplayed()) {
            return RestrictionMode.ANYONE_CAN_VIEW_AND_EDIT;
        } else if ($(restrictionsButtonSelector + "[aria-label='Editing restricted']").isDisplayed()){
            return RestrictionMode.ANYONE_CAN_VIEW_SOME_CAN_EDIT;
        } else if ($(restrictionsButtonSelector + "[aria-label='Restrictions apply']").isDisplayed()) {
            return RestrictionMode.ONLY_SOME_CAN_VIEW_AND_EDIT;
        } else {
            throw new IllegalStateException("Restriction mode is not found");
        }
    }

    @Step("Open restriction modification pop-up")
    private RestrictionsPopUp modifyRestrictions() {
        $("[data-test-id='restrictions.dialog.button']").click();
        return new RestrictionsPopUp();
    }

    @Override
    public String getTitle() {
        return $("[data-test-id='title-text']").text();
    }
}
