package com.atlassian.pages.confluence;

import io.qameta.allure.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.codeborne.selenide.Selenide.$;

public class ConfluenceSpaceHomePage implements ConfluenceBasePage {
    public static final Logger log = LoggerFactory.getLogger(ConfluenceSpaceHomePage.class);

    // TODO: this is not optimal solution, but it's selected to simplicity. It's better to open page using global search
    @Step("Open page '{name}'")
    public ConfluencePage openPage(final String name) {
        log.info("Open page {}", name);
        return openPages().search(name);
    }

    @Step("Open Confluence pages")
    private ConfluencePagesPage openPages() {
        $("[data-testid='pageTree'] [data-item-title=true]").click();
        return new ConfluencePagesPage();
    }

    @Override
    public String getTitle() {
        return $("[data-test-id='title-text']").text();
    }
}
