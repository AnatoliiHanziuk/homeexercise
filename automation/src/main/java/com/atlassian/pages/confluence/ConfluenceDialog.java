package com.atlassian.pages.confluence;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public interface ConfluenceDialog {
    Logger log = LoggerFactory.getLogger(ConfluenceDialog.class);

    default SelenideElement getDialog() {
        return $("#com-atlassian-confluence [role='dialog'][aria-modal='true']");
    }

    default SelenideElement getDialogHeader() {
        return getDialog().$("h1");
    }

    default SelenideElement getApplyButton() {
        return getDialog().$(byText("Apply"));
    }

    default String getDialogName() {
        return getDialogHeader().text();
    }

    @Step("Apply changes")
    default void applyChanges() {
        log.info("Apply changes");
        getApplyButton().click();
    }

    @Step("Cancel changes")
    default void cancelChanges() {
        log.info("Cancel changes");
        getDialog().$(byText("Cancel")).click();
    }
}
