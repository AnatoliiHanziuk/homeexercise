package com.atlassian.pages.confluence;

import com.atlassian.RestrictionMode;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.withText;

public class RestrictionsPopUp implements ConfluenceDialog {

    @Step("Apply new restriction")
    public ConfluencePage selectRestriction(final RestrictionMode mode) {
        selectRestriction(mode.get()).applyChanges();
        return new ConfluencePage();
    }

    @Step("Select restriction '{modeName}'")
    private RestrictionsPopUp selectRestriction(final String modeName) {
        final String contentModeSelector = "[data-test-id='restrictions-dialog.content-mode-select']";
        getDialog().$(contentModeSelector).click();
        getDialog().$(contentModeSelector).find(withText(modeName)).click();
        return this;
    }
}
