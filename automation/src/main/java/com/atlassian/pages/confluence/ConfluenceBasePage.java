package com.atlassian.pages.confluence;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.refresh;

public interface ConfluenceBasePage {
    @Step("Get page title")
    String getTitle();

    @Step("Reload page")
    default ConfluenceBasePage reload() {
        refresh();
        return this;
    }
}
