package com.atlassian.extensions;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimingExtension implements BeforeTestExecutionCallback, AfterTestExecutionCallback {
    public static final Logger log = LoggerFactory.getLogger(TimingExtension.class);
    private static final String START_TIME = "start time";
    private long methodDuration;

    @Override
    public void beforeTestExecution(ExtensionContext context) {
        getMethodStore(context).put(START_TIME, System.currentTimeMillis());
    }

    @Override
    public void afterTestExecution(ExtensionContext context) {
        long startTime = getMethodStore(context).remove(START_TIME, long.class);
        setMethodDuration(System.currentTimeMillis() - startTime);

        log.info("Method {} took {} ms.", context.getRequiredTestMethod().getName(), methodDuration);
    }

    public long getMethodDuration() {
        return methodDuration;
    }

    public void setMethodDuration(long methodDuration) {
        this.methodDuration = methodDuration;
    }

    private ExtensionContext.Store getMethodStore(ExtensionContext context) {
        return context.getStore(ExtensionContext.Namespace.create(getClass(), context.getRequiredTestMethod()));
    }
}

