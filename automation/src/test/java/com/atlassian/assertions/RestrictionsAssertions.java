package com.atlassian.assertions;

import com.atlassian.RestrictionMode;
import io.qameta.allure.Step;

import static org.assertj.core.api.Assertions.assertThat;

public class RestrictionsAssertions {
    @Step("Check selected restriction mode for the page")
    public void checkRestrictionMode(final RestrictionMode actualRestrictionMode, final RestrictionMode expectedRestrictionMode) {
        assertThat(actualRestrictionMode)
                .as(String.format("Actual restriction mode '%s' is not equal to expected '%s'", actualRestrictionMode.get(), expectedRestrictionMode.get()))
                .isEqualTo(expectedRestrictionMode);
    }
}
