package com.atlassian.tests;

import com.atlassian.config.AllureConfigurator;
import com.atlassian.config.SelenideConfigurator;
import com.atlassian.extensions.AllureSelenideListener;
import com.atlassian.extensions.TestLogging;
import com.atlassian.extensions.TimingExtension;
import com.atlassian.pages.atlassian.AtlassianLoginPage;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.junit5.AllureJunit5;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.codeborne.selenide.Selenide.page;

@TestLogging
@ExtendWith(TimingExtension.class)
@ExtendWith(AllureJunit5.class)
public class BaseConfluenceTest {
    public static final SelenideConfigurator selenide = new SelenideConfigurator();
    public static final AllureConfigurator allure = new AllureConfigurator();

    @BeforeAll
    public static void setUp() {
        selenide.configure();
        SelenideLogger.addListener("Allure Selenide", new AllureSelenideListener());
        openConfluence();
    }

    @AfterAll
    public static void tearDown() {
        allure.addEnvInfoToReport();
        SelenideLogger.removeListener("Allure Selenide");
        WebDriverRunner.closeWebDriver();
    }

    public static <T> T at(final Class<T> clazz) {
        return page(clazz);
    }

    public static void openConfluence() {
        at(AtlassianLoginPage.class)
                .open()
                .login(System.getProperty("email"), System.getProperty("password"))
                .openConfluence();
    }
}
