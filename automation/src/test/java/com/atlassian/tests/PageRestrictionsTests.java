package com.atlassian.tests;

import com.atlassian.RestrictionMode;
import com.atlassian.assertions.RestrictionsAssertions;
import com.atlassian.pages.confluence.ConfluenceHomePage;
import com.atlassian.pages.confluence.ConfluencePage;
import io.qameta.allure.Description;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

public class PageRestrictionsTests extends BaseConfluenceTest {
    public final RestrictionsAssertions restrictionAssertions = new RestrictionsAssertions();

    @BeforeEach
    void openPage() {
        // given
        at(ConfluenceHomePage.class)
                .openSpace(System.getProperty("space"))
                .openPage(System.getProperty("page"));
    }

    @ParameterizedTest
    @EnumSource(RestrictionMode.class)
    @Description("Can modify page restriction mode")
    void canModifyPageRestrictionMode(final RestrictionMode mode) {
        // when
        at(ConfluencePage.class).setRestriction(mode);
        var actualRestrictionMode = at(ConfluencePage.class).getRestrictionMode();

        // then
        restrictionAssertions.checkRestrictionMode(actualRestrictionMode, mode);
    }
}
