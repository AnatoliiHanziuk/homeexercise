package com.atlassian.config;

import org.aeonbits.owner.Config;

@Config.Sources({"classpath:browser.properties"})
public interface BrowserConfig extends Config {
    @Key("headless")
    boolean headless();

    @Key("browserSize")
    String browserSize();
}
