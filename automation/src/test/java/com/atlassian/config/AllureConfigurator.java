package com.atlassian.config;

import com.codeborne.selenide.Configuration;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static com.atlassian.utils.FileUtils.writeToFile;

public class AllureConfigurator {

    public void addEnvInfoToReport() {
        var envInfo = List.of(
                "URL: " + Configuration.baseUrl,
                "Browser: " + Configuration.browser + " " + Configuration.browserVersion,
                "Browser.size: " + Configuration.browserSize,
                "Browser.headless: " + Configuration.headless
        );
        writeToFile("build/allure-results/environment.properties", envInfo, StandardCharsets.UTF_8);
    }
}
