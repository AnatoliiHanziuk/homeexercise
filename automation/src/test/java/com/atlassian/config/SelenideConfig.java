package com.atlassian.config;

import org.aeonbits.owner.Config;

@Config.Sources({"classpath:selenide.properties"})
public interface SelenideConfig extends Config {
    @Key("pageLoadStrategy")
    String pageLoadStrategy();

    @Key("timeout")
    long timeout();

    @Key("pageLoadTimeout")
    long pageLoadTimeout();
}
