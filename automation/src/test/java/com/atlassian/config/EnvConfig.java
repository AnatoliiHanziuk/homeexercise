package com.atlassian.config;

import org.aeonbits.owner.Config;

@Config.Sources({"classpath:env.properties"})
public interface EnvConfig extends Config {
    @Config.Key("url")
    String url();
}
