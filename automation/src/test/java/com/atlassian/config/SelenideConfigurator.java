package com.atlassian.config;

import com.codeborne.selenide.Configuration;
import org.aeonbits.owner.ConfigFactory;

public class SelenideConfigurator {

    public static final EnvConfig ENV_CONFIG = ConfigFactory.create(EnvConfig.class);
    public static final SelenideConfig SELENIDE_CONFIG = ConfigFactory.create(SelenideConfig.class);
    public static final BrowserConfig BROWSER_CONFIG = ConfigFactory.create(BrowserConfig.class);
    public static final String BROWSER_NAME = System.getProperty("browser").split(":")[0];
    public static final String BROWSER_VERSION = System.getProperty("browser").split(":")[1];

    public void configure() {
        Configuration.baseUrl = ENV_CONFIG.url();
        Configuration.browser = BROWSER_NAME;
        Configuration.browserVersion = BROWSER_VERSION;
        Configuration.timeout = SELENIDE_CONFIG.timeout();
        Configuration.pageLoadTimeout = SELENIDE_CONFIG.pageLoadTimeout();
        Configuration.pageLoadStrategy = SELENIDE_CONFIG.pageLoadStrategy();
        Configuration.headless = BROWSER_CONFIG.headless();
        Configuration.browserSize = BROWSER_CONFIG.browserSize();
    }
}
