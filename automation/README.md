## Pre-conditions
You need to have  
- JDK11 installed (https://sdkman.io/jdks)  
- Installed a browser of version you're going to test on  
- Registered in Atlassian having access to Confluence  
- Have already created space and page in Confluence  


## How to execute tests
`./gradlew clean test -Demail=<yourAtlassianEmail> -Dpassword=<yourAtlassianPassword> -Dspace=<yourConfluenceSpaceName>`  
This command has several optional arguments:  
`-Dbrowser=<browserName:browserVersion>`  
`-Dpage=<yourConfluencePageName>`

## How to see a report
`./gradlew allureServe`  
Execution on this command will open the report in your default browser

## What next steps should be done to improve the solution
- Start to execute tests continuously (e.g. in CI pipeline) so that they'll bring value
- Add Dockerfile
- Allow test execution in the dockerized browsers (e.g. use Selenoid and start it as a docker-compose file)
- Pair programming with the members of development team to make sure they're capable to expand existing solution
- Simplify the navigation (e.g. navigate to Space or Page using the URI instead of a series of clicks)