## Mindset exercise

Answers are in `/mindset` directory.

## Exploratory testing exercise

Answers are in `/exploratory` directory.

## Automation exercise

Answers are in `/automation` directory.