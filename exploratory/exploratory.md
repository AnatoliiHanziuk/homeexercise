I decided to do 3 types of verifications:  
- understand whether the functionality works in terms of a specific page and what kind of the permission restrictions are available in Confluence.  
- how this functionality works on different levels (pages/sub-pages, pages/spaces relationships)  
- what happens if the person is a part of multiple groups  

Each of the verification types was timeboxed with 10 minutes.

- Check that by default the page is visible and available to be edited to all users
- Chek "can edit" changes to "can view" if I add new permission to view the page for a user that is already able to edit it
- [Potential issue] Check that I can search for all "admin" groups in "type username or group" field. I'm not able to do it unless the "admin" is a beginning of separate work in a group or username
- Check I can remove already added access
- Check I cannot remove all accesses to a page. The page should always have somebody who can edit it
- Check that non-applied changes are not saved
- Check the icon is changed on the page level according to the selected restriction mode
- Check there is no possibility to select "can view" in "anyone can view, some can edit" mode
- Check there is no possibility to select anything in "anyone can view and edit" mode

I figured out that it's possible to modify permission restriction not only on the level of the page, but also on the level of the space.  
- Check if the person cannot view the space then it cannot view the page  
- Check if person can view the space, but cannot edit it, it's still able by default to edit the page in this space  

Also, it's possible to create sub-pages.  
- Check there are inherited vew restrictions notification is present and people that are restricted to view parent page cannot view the child page.   
- Check if the person isn't able to edit the parent page but can view it, by default it should be able to edit the child page.  

I tried to add a person P to 2 different groups (A and B)  
- Check if neither of groups A, B don't have permission to view the page, then the person P from these groups cannot view it  
- Check if neither of groups A, B don't have permission to view the page, but the person P has permissions to view the page, then he can view it  
- Check if group A does not have permissions to view the page and group B has permissions to do it, then person P is able to view the page  

Further testing areas:
- What should happen if the user doesn't have access to the page, and he receives the link to it - should he have the possibility to request access?  
- Whether person that doesn't have access to page receives notifications if there is some information about him in this page (e.g. assigned task or mention)  
- Possibility to grant access to a page for a user that doesn't have Confluence account (shared public link)  
